import qbs

Project {
    StaticLibrary{
        //name:"serialLib"
        Depends{name:"cpp"}

        targetName:{
            if(qbs.buildVariant == "debug")
                return "serial-mtd"
            else
                return "serial-mt"
        }

        files:[
            "serial/include/serial/serial.h",
            "serial/include/serial/v8stdint.h",
            "serial/src/serial.cc",
        ]

        Group{
            condition: qbs.targetOS.contains("linux")
            name: "linux"
            files:[
                "serial/src/impl/list_ports/list_ports_linux.cc",
                "serial/src/impl/unix.cc",
            ]
        }
        Group{
            condition: qbs.targetOS.contains("windows")
            name: "windows"
            files:[
                "serial/src/impl/list_ports/list_ports_win.cc",
                "serial/src/impl/win.cc",
            ]
        }

        cpp.includePaths:["serial/include"]
        cpp.dynamicLibraries:{
            var dyn_libs = base
            if(qbs.targetOS.contains("linux")) {
                dyn_libs = dyn_libs.concat(["pthread", "rt"])
            }//linux

            return dyn_libs
        }
        cpp.cxxFlags:{
            var flags = base
            if(cpp.compilerName.contains("g++"))
                flags = flags.concat(["-std=gnu++11","-pthread" ])
            return flags
        }

        Group {
            name: "The App itself"
            fileTagsFilter: product.type
            qbs.install: true
            qbs.installDir: {
                var dir = "lib/"

                if(qbs.targetOS.contains("linux"))
                    dir = dir + "linux/"
                dir = dir + qbs.architecture
                return dir
            }
            }
    }
}

