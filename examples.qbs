import qbs

Project {
    CppApplication{
        Depends{name : "libstatic"}
        targetName:"serial_example"
        files:[
            "serial/examples/serial_example.cc",
        ]
        cpp.includePaths:["serial/include"]
        //cpp.runtimeLibrary:"static"

        Group {
            name: "The App itself"
            fileTagsFilter: product.type
            qbs.install: true
            qbs.installDir: "bin"
            }

        cpp.rpaths : {
            var dir = "../lib/"

            if(qbs.targetOS.contains("linux"))
                dir = dir + "linux/"
            dir = dir + qbs.architecture
            var rpaths = base
            rpaths = rpaths.concat([".",dir])
            return rpaths
        }
        cpp.dynamicLibraries:{
            var dyn_libs = base
            if(qbs.hostOS.contains("linux")) {
                dyn_libs = dyn_libs.concat(["pthread", "rt"])
            }//linux
            if(qbs.hostOS.contains("windows")) {
                dyn_libs = dyn_libs.concat(["setupapi", "Advapi32"])
            }

            return dyn_libs
        }
        cpp.cxxFlags:{
            var flags = base
            if(cpp.compilerName.contains("g++"))
                flags = flags.concat(["-std=gnu++11","-pthread" ])
            return flags
        }
    }
}

