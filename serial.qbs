import qbs

Project {
    references: [
        "./lib.qbs",
        "./libstatic.qbs",
        "./examples.qbs",
        ]
}
